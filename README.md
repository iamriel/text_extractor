# Text Extractor #

Text Extractor is a simple python tool for extracting texts from PDF and PPTX documents.

### What is this repository for? ###

* Used for extracting text in a simple way from PDF and PPTX documents.
* Uses [pdfminer](https://github.com/euske/pdfminer) and [python-pptx](https://github.com/scanny/python-pptx)
* 1.0

### How do I get set up? ###

* Install Python 2.6 or newer. (Python 3 is not supported.)
* Download the source code.
* Unpack it.
* Run `python setup.py install`

### Example Usage ###

~~~~{.python}
from text_extractor import extractor

filename = 'example.pptx'
text = extractor.get_text(filename)  # Returns a string

# Take note that get_text method will automatically identify if it is pdf or pptx
# It will raise TypeError if the file you specified is not pdf or pptx

filename = 'example.pdf'
text = extractor.get_text(filename)  # Returns a string
~~~~

### Running Tests ###
* Run `python setup.py test`