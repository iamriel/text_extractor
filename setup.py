from setuptools import setup


def readme():
    with open('README.md') as f:
        return f.read()


setup(
    name='text_extractor',
    version='0.1',
    description='Extracts text from pdf and pptx',
    long_description=readme(),
    url='https://bitbucket.org/iamriel/text_extractor',
    author='Rieljun Liguid',
    author_email='me@iamriel.com',
    license='MIT',
    packages=['text_extractor'],
    include_package_data=True,
    install_requires=[
        'pdfminer',
        'python-pptx',
    ],
    zip_safe=False,
    test_suite='nose.collector',
    tests_require=['nose'],
)