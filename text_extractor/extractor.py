import os
import re

from cStringIO import StringIO
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage

from pptx import Presentation


def extract_pdf_text(fname, pages=None):
    """
        Extracts text from a pdf file
    """
    if not pages:
        pagenums = set()
    else:
        pagenums = set(pages)

    output = StringIO()
    manager = PDFResourceManager()
    converter = TextConverter(manager, output, laparams=LAParams())
    interpreter = PDFPageInterpreter(manager, converter)

    infile = file(fname, 'rb')
    for page in PDFPage.get_pages(infile, pagenums):
        interpreter.process_page(page)
    infile.close()
    converter.close()
    text = output.getvalue()
    output.close
    return text


def extract_pptx_text(fname, pages=None):
    """
        Extracts text from a pptx file
    """
    prs = Presentation(fname)

    text = ''

    for slide in prs.slides:
        for shape in slide.shapes:
            if not shape.has_text_frame:
                continue
            for paragraph in shape.text_frame.paragraphs:
                text += paragraph.text + ' '

    return text


def clean_text(text):
    """
        Removes extra whitespace including tabs and newlines
    """
    return re.sub('\s+', ' ', text).strip()


def get_text(fname, **kwargs):
    """
        Extracts text from a file

        Keyword arguments:
            `pages`: int, number of pages to be extracted
                if None, all pages will be extracted
            `clean`: boolean, removes extra whitespace from the result

    """
    supported_filetypes = ('.pdf', '.pptx')
    ext = os.path.splitext(fname)[1]

    if ext not in supported_filetypes:
        raise TypeError('File not supported. %s files only' % str(supported_filetypes))

    pages = kwargs.get('pages', None)
    clean = kwargs.get('clean', False)

    if ext == '.pdf':
        text = extract_pdf_text(fname, pages)
    else:
        text = extract_pptx_text(fname, pages)

    if clean:
        return clean_text(text)
    return text

    